const path = require('path');
const DashboardPlugin = require('webpack-dashboard/plugin');


module.exports = {
  entry: [
    'babel-polyfill',
    './src/index.js',
  ],
  output: {
    path: path.resolve(__dirname, 'www'),
    filename: 'bundle.js',
  },
  module: {
    rules: [{
      test: /\.js$/,
      use: [{
        loader: 'babel-loader',
        options: {
          presets: ['es2015', 'react', 'stage-2'],
          plugins: ['transform-decorators-legacy'],
          cacheDirectory: true,
        },
      }],
      include: path.resolve(__dirname, 'src'),

    }],
  },
  plugins: [
    new DashboardPlugin(),
  ],
  resolve: {
    modules: ['node_modules', './src'],
  },
  watchOptions: {
    ignored: /node_modules/,
  },
  cache: true,
  devtool: 'cheap-module-eval-source-map',
  devServer: {
    contentBase: './www',
    compress: true,
    port: 8000,
    historyApiFallback: true,
    host: '0.0.0.0',
  },
};
