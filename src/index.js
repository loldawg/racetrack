import React from 'react';
import ReactDOM from 'react-dom';
import Main from 'components/main';
import Store from 'store/store';
import { Provider } from 'react-redux';


ReactDOM.render(
  <Provider store={ Store }>
    <Main />
  </Provider>,
  document.getElementById('root')
);
