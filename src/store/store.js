import { createStore, applyMiddleware } from 'redux';
import thunk from 'redux-thunk';
import * as Reducers from 'store/reducers.js';

const defaultState = {
  cars: [],
  speedLimits: [],
  trafficLights: [],
  distance: 0,
  /* raceState: pending - before it starts; inProgress - during the race,
  finished - after the race */
  raceState: 'pending',
  raceCounter: 0,
  raceWinners: [],
  selectedCars: [],
};

function reducer(state = defaultState, action) {
  if (Reducers[action.type]) {
    return Reducers[action.type](state, action);
  } else if (action.type === '@@redux/INIT') {
    return state;
  }
  console.log('Unknown action: ', action.type);
  return state;
}


export default createStore(reducer, applyMiddleware(thunk));
