export const STORE_DATA = (state, action) => {
  return {
    ...state,
    distance: action.data.distance,
    cars: action.data.cars,
    speedLimits: action.data.speed_limits,
    trafficLights: action.data.traffic_lights,
  };
};

export const TOGGLE_CAR = (state, action) => {
  let pos = null;
  state.selectedCars.forEach((car, index) => {
    if (car.id === action.data.id) {
      pos = index;
    }
  });
  if (pos !== null) {
    return {
      ...state,
      selectedCars: [
        ...state.selectedCars.slice(0, pos),
        ...state.selectedCars.slice(pos + 1),
      ],
    };
  }
  return {
    ...state,
    selectedCars: [...state.selectedCars, action.data],
  };
};

export const SET_RACE = (state, action) => {
  return {
    ...state, raceState: action.state,
  };
};

export const ADD_WINNER = (state, action) => {
  return {
    ...state,
    raceWinners: [...state.raceWinners, action.data],
  };
};

export const RESET_RACE = (state) => {
  return {
    ...state,
    selectedCars: [

    ],
    raceWinners: [

    ],
  };
};
