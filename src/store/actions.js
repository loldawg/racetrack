export const storeData = (resJson) => {
  return {
    type: 'STORE_DATA',
    data: resJson,
  };
};

export const dataGrab = () => {
  return (dispatch) => {
    fetch('/data.json', {
      method: 'GET',
      headers: {
        'Accept': 'application/json',
        'Content-Type': 'application/json',
      },
    }).then((res) => res.json())
      .then((resJson) => {
        dispatch(storeData(resJson));
      });
  };
};

export const toggleCar = (car) => {
  return {
    type: 'TOGGLE_CAR',
    data: car,
  };
};
