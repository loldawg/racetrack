import React, { Component } from 'react';
import injectSheet from 'react-jss';
import Carpicker from 'components/carpicker';
import Track from 'components/track';
import { dataGrab } from 'store/actions';
import { connect } from 'react-redux';
import PropTypes from 'prop-types';


const styles = {
  main: {
    width: '100%',
    margin: '0 auto',
  },
};

@injectSheet(styles)
@connect()
class Main extends Component {
  static propTypes = {
    dispatch: PropTypes.func,
    classes: PropTypes.object,
  }
  componentDidMount() {
    this.props.dispatch(dataGrab());
  }
  render() {
    return (
      <div className={ this.props.classes.main } >
        <Carpicker />
        <Track />
      </div>
    );
  }
}

export default Main;
