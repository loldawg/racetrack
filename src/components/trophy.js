import React, { Component } from 'react';
import injectSheet from 'react-jss';
import classNames from 'classnames';
import PropTypes from 'prop-types';


const styles = {
  trophy: {
    position: 'absolute',
    right: -40,
    background: '#FFF',
    padding: 7,
    borderRadius: 15,
  },
  gold: {
    fill: '#f1c40f',
  },
  silver: {
    fill: '#bdc3c7',
  },
  bronze: {
    fill: '#d35400',
  },
};

@injectSheet(styles)

class Trophy extends Component {
  static propTypes = {
    classes: PropTypes.object,
    color: PropTypes.string,
  }
  render() {
    const { classes } = this.props;
    return (
      <svg
        width='16px'
        height='16px'
        viewBox='0 0 16 16'
        version='1.1'
        xmlns='http://www.w3.org/2000/svg'
        xmlnsXlink='http://www.w3.org/1999/xlink'
        className={ classNames({
          [classes.trophy]: true,
          [classes.gold]: this.props.color === 'gold',
          [classes.silver]: this.props.color === 'silver',
          [classes.bronze]: this.props.color === 'bronze',
        })
        }
      >
        <path d='M11.7,8 C15.9,7.7 16,5.3 16,3 L13,3 L13,0 L3,0 L3,3 L0,3 C0,5.3 0.1,7.7 4.3,8 C5.2,9.4 6.4,10 7,10 L7,14 C4,14 4,16 4,16 L12,16 C12,16 12,14 9,14 L9,10 C9.6,10 10.8,9.4 11.7,8 Z M13,4 L15,4 C14.9,5.6 14.6,6.7 12.3,6.9 C12.6,6.1 12.9,5.2 13,4 Z M1,4 L3,4 C3.1,5.2 3.4,6.1 3.7,6.9 C1.5,6.7 1.1,5.6 1,4 Z M4.5,6.1 C4,4.4 4,3 4,3 L4,1 L5,1 L5,3 C5,3 5,4.7 5.4,6.1 C5.9,7.8 7,9 7,9 C7,9 5.2,8.8 4.5,6.1 Z' />
      </svg>
    );
  }
}

export default Trophy;
