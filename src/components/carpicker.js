import React, { Component } from 'react';
import injectSheet from 'react-jss';
import { connect } from 'react-redux';
import { toggleCar } from 'store/actions';
import classNames from 'classnames';
import PropTypes from 'prop-types';

const styles = {
  searchbox: {
    display: 'block',
    border: 'none',
    width: '80%',
    height: 40,
    borderRadius: 10,
    outline: 'none',
    margin: '0 auto',
    padding: 10,
    '&:focus': {
      background: 'pink',
    },
  },
  carholder: {
    display: 'flex',
    flexWrap: 'wrap',
    justifyContent: 'space-between',
    position: 'relative',
    marginTop: 60,
  },
  normal: {
    opacity: 0.2,
    cursor: 'pointer',
    flex: 1,
    width: 150,
    margin: '10px 0 0 10px',
    backgroundColor: '#fff',
    borderRadius: 10,
    position: 'relative',
    transition: 'all 0.5s ease-in-out 0s',
  },
  active: {
    opacity: 1,
  },
  selected: {
    backgroundColor: '#2ecc71',
    color: '#fff',
  },
  carpicture: {
    width: 150,
    display: 'block',
    bottom: 0,
    margin: '0 auto',
    padding: '0 0 10px 0',
  },
  cartitle: {
    fontSize: '20px',
    padding: 10,
    textAlign: 'center',
    fontWeight: 'bold',
  },
  additional: {
    opacity: 0,
    background: 'rgba(55,53,53, 0.8);',
    position: 'absolute',
    top: 0,
    outline: 'none',
    height: '100%',
    width: '100%',
    borderRadius: 10,
    color: '#fff',
    transition: 'all 0.5s ease-in-out 0s',
    '&:hover': {
      opacity: 1,
    },
  },
  cardesc: {
    padding: 10,
  },
  carspeed: {
    padding: '5px 10px 0 10px',
  },
};

@injectSheet(styles)
@connect((state) => {
  return {
    cars: state.cars,
    selected: state.selectedCars,
  };
})
class Carpicker extends Component {
  static propTypes = {
    dispatch: PropTypes.func,
    selected: PropTypes.array,
    classes: PropTypes.object,
    cars: PropTypes.array,
  }

  constructor() {
    super();
    this.state = {
      search: '',
    };
  }
  pickCar = (car) => {
    this.props.dispatch(toggleCar(car));
  }
  findCar = (car) => {
    for (let i = 0; i < this.props.selected.length; i++) {
      if (this.props.selected[i].id === car.id) {
        return true;
      }
    }
    return false;
  }
  render() {
    const { classes, cars } = this.props;
    return (
      <div>
        <input
          type='search'
          placeholder='Search cars'
          className={ classes.searchbox }
          onChange={ (event) => this.setState({ search: event.target.value }) }
          value={ this.state.search }
        />
        <div className={ classes.carholder } >
          {cars.map((car) => {
            return (
              <div
                key={ car.id }
                className={ classNames({
                  [classes.normal]: true,
                  [classes.active]: car.name.toLowerCase()
                  .match(this.state.search.toLowerCase()),
                  [classes.selected]: this.findCar(car),
                }) }
              >
                <div className={ classes.cartitle }>{ car.name }</div>
                <img alt='Car avatar' src={ car.image } className={ classes.carpicture } />
                <button className={ classes.additional } onClick={ () => this.pickCar(car) }>
                  <div className={ classes.cardesc }>{ car.description }</div>
                  <div className={ classes.carspeed }>Speed: { car.speed }</div>
                </button>
              </div>
    );
  })}
        </div>
      </div>
    );
  }
}

export default Carpicker;
