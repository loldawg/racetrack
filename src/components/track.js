import React, { Component } from 'react';
import injectSheet from 'react-jss';
import { connect } from 'react-redux';
import classNames from 'classnames';
import PropTypes from 'prop-types';
import Trophy from './trophy';


const styles = {
  main: {
    width: 1000,
    margin: '0 auto',
    marginTop: 50,
  },
  signs: {
    height: 50,
    position: 'relative',
  },
  track: {
    display: 'flex',
    position: 'relative',
  },
  field: {
    height: 50,
    flex: 1,
    border: '1px solid rgba(55,53,55, .5)',
  },
  racecar: {
    position: 'absolute',
    left: 5,
    top: 5,
    height: 40,
    transform: 'translateX(-50%)',
  },
  button: {
    display: 'block',
    margin: '0 auto',
    width: 300,
    height: 40,
    color: '#fff',
    backgroundColor: '#3498db',
    textAlign: 'center',
    lineHeight: '40px',
    borderRadius: 30,
    cursor: 'pointer',
    marginBottom: 50,
    outline: 'none',
  },
  icon: {
    display: 'block',
    width: 25,
    height: 25,
    color: 'black',
    position: 'absolute',
  },
  disabled: {
    backgroundColor: '#696767',
  },
  reset: {
    backgroundColor: 'red',
  },
};


@injectSheet(styles)
@connect((state) => {
  return {
    selected: state.selectedCars,
    traffic: state.trafficLights,
    distance: state.distance,
    limits: state.speedLimits,
    raceState: state.raceState,
    raceWinners: state.raceWinners,
  };
})

class Track extends Component {
  static propTypes = {
    classes: PropTypes.object,
    distance: PropTypes.number,
    selected: PropTypes.array,
    dispatch: PropTypes.func,
    raceState: PropTypes.string,
    limits: PropTypes.array,
    traffic: PropTypes.array,
    raceWinners: PropTypes.array,
  }
  constructor() {
    super();
    this.state = {
      currentPos: [],
      // lightState: {},
      finished: {},
    };
  }

  speedLimit(position, speed, index) {
    return (
      <div
        style={ {
          position: 'absolute',
          left: ((1000 / this.props.distance) * position) - 20,
          width: 40,
          height: 40,
          border: '1px solid red',
          borderRadius: 50,
          textAlign: 'center',
          lineHeight: '40px',
          backgroundColor: 'red',
          color: '#fff',
          fontWeight: 'bold',
        } }
        key={ index }
      >
        {speed}
      </div>
    );
  }
  trafficLights(position, duration, index) {
    return (
      <div
        style={ {
          position: 'absolute',
          left: ((1000 / this.props.distance) * position) - 20,
          width: 40,
          height: 40,
        } }
        key={ index }
      />
    );
  }
  startRace = () => {
    this.setState({
      currentPos: this.props.selected.map(() => 0),
    }, () => {
      this.props.dispatch({ type: 'SET_RACE', state: 'started' });
      this.interval = setInterval(() => {
        const newPos = this.state.currentPos.map((pos, index) => {
          if (this.state.finished[index]) {
            return pos;
          }
          const currentCar = this.props.selected[index];
          let calcPos = pos + (currentCar.speed / 100);
          if (calcPos >= 1000) {
            calcPos = 1000;
            this.setState({
              finished: { ...this.state.finished, [index]: true },
            });
            this.props.dispatch({ type: 'ADD_WINNER', data: currentCar.id });
          }
          return calcPos;
        });
        const moving = this.state.currentPos.filter((pos) => pos < 1000);
        if (moving.length === 0) {
          clearInterval(this.interval);
          this.props.dispatch({ type: 'SET_RACE', state: 'finished' });
        }
        this.setState({
          currentPos: newPos,
        });
      }, 1);
    });
  }
  resetRace = () => {
    this.setState({
      currentPos: [],
      finished: {},
    }, () => {
      this.props.dispatch({ type: 'SET_RACE', state: 'pending' });
      this.props.dispatch({ type: 'RESET_RACE' });
    });
  }

  renderButton(raceState) {
    const { classes } = this.props;
    let label;
    let cnames = {};
    let clickaction;
    if (raceState === 'pending') {
      label = 'Start race';
      cnames = {
        [classes.button]: true,
      };
      clickaction = this.startRace;
    } else if (raceState === 'started') {
      label = 'Start race';
      cnames = {
        [classes.button]: true,
        [classes.disabled]: true,
      };
    } else {
      label = 'Reset race';
      cnames = {
        [classes.button]: true,
        [classes.reset]: true,
      };
      clickaction = this.resetRace;
    }

    return (
      <button
        className={ classNames(cnames) }
        onClick={ clickaction }
        disabled={ this.state.raceState === 'started' }
      >{label}
      </button>
    );
  }

  render() {
    const { classes, raceState } = this.props;
    return (
      <div className={ classes.main }>
        {this.renderButton(raceState)}
        <div className={ classes.signs }>
          {this.props.limits.map((limit, index) => this.speedLimit(
            limit.position,
            limit.speed,
            index
          ))}
          {this.props.traffic.map((traffic, index) => this.trafficLights(
            traffic.position,
            traffic.duration,
            index
          ))}
        </div>
        {this.props.selected.map((car, index) => {
            return (
              <div className={ classes.track } key={ car.id }>
                <img
                  alt='Car avatar'
                  src={ car.image }
                  className={ classNames({
                    [classes.racecar]: true,
                  }) }
                  style={ { left: this.state.currentPos[index] } }
                />
                {this.props.raceWinners[0] === car.id &&
                  <Trophy color='gold' />
                }
                {this.props.raceWinners[1] === car.id &&
                  <Trophy color='silver' />
                }
                {this.props.raceWinners[2] === car.id &&
                  <Trophy color='bronze' />
                }
                {[1, 2, 3, 4, 5, 6, 7, 8, 9, 10].map((item) => {
                return (
                  <div className={ classes.field } key={ item } />
                );
              })}
              </div>
            );
          })}
      </div>
    );
  }
}

export default Track;
